.. testing exhale documentation master file, created by
   sphinx-quickstart on Mon Aug  2 12:22:04 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to testing exhale's documentation!
==========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   about
   api/library_root


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
